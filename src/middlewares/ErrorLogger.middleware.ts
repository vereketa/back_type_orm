import {Service} from "typedi";
import {ArgumentValidationError, MiddlewareInterface, NextFn, ResolverData} from "type-graphql";
import {Context} from "../types/Context.interface";
import {LoggerSingle} from "../services/Logger.service";

@Service()
export class ErrorLoggerMiddleware implements MiddlewareInterface<Context> {
  constructor(private readonly logger: LoggerSingle) {}

  async use({context, info}: ResolverData<Context>, next: NextFn) {
    try {
      return await next();
    }
    catch (err) {
      this.logger.log(`
        message: ${err.message},\n
        operation: ${info.operation.operation},\n
        fieldName: ${info.fieldName},\n
        userName: ${context.account.id},
      `);
      if(!(err instanceof ArgumentValidationError)) {
        throw new Error("Unknown error occurred. Try again later!");
      }
      throw err;
    }
  }
}
