import {MiddlewareFn, ResolverData} from "type-graphql";

export const CountMiddleware: MiddlewareFn =
    async ({info}: ResolverData<any>, next) => {
      const res = await next();
      if(Array.isArray(res)) {
        return {
          data: res,
          count: res.length,
        }
      }
      return res;
    };
