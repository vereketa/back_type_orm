import {MiddlewareFn} from "type-graphql";

export const CompetitorDetector: MiddlewareFn = async ({args}, next) => {
  if(args.id === '1') {
    console.log(args);
    return 'User not found';
  }
  return next();
};
