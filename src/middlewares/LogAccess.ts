import {MiddlewareInterface, NextFn, ResolverData} from "type-graphql";
import {Context} from "../types/Context.interface";
import {Service} from "typedi";
import {LoggerSingle} from "../services/Logger.service";

@Service()
export class LogAccessMiddleware implements MiddlewareInterface<Context> {
  constructor(private readonly logger: LoggerSingle) {}

  async use({context, info}: ResolverData<Context>, next: NextFn) {
    this.logger.log(
        `Logging access: ${context.account.id} -> ${info.parentType.name}.${info.fieldName}`,
    );
    return next();
  }
}
