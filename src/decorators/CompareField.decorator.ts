import {registerDecorator, ValidationOptions} from "class-validator";
import {CustomCompare} from "../validators/CustomCompare";
import {ICompareValidationOptions} from "../types/ICompareValidationOptions.interface";

export function CompareField(validationOptions?: ICompareValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [validationOptions.compareName],
      validator: CustomCompare,
    })
  }
}