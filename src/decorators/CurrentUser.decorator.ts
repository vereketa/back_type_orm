import {createParamDecorator} from "type-graphql";
import {Context} from "../types/Context.interface";

export default function CurrentUser() {
  return createParamDecorator<Context>(({context}) => {
    return context.account;
  })
}
