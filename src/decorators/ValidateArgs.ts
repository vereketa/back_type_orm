import {createMethodDecorator, ClassType, ArgumentValidationError} from "type-graphql";
import {validate} from 'class-validator'
import {plainToClass} from "class-transformer";

export function ValidateArgs<T extends object>(type: ClassType<T>) {
  return createMethodDecorator(async ({ args }, next) => {
    const instance = plainToClass(type, args);
    const validationErrors = await validate(instance);
    if(validationErrors.length > 0) {
      throw new ArgumentValidationError(validationErrors);
    }
    return next();
  })
}
