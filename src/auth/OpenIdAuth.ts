import passport from "passport";
import {Profile} from "passport-facebook";
import {AccountService} from "../graphql/account/Account.service";
import {getUrlAuth, getUrlCallback, getUrlFull} from "../util/getUrlAuth";
import config from '../../config';
import app from '../server/app';
const {Strategy: OpenIDStrategy} = require('passport-openid');


const includeOpenId = () => {
  passport.use(new OpenIDStrategy({
      returnURL: getUrlFull('openid'),
      realm: config.base_url,
      profile: true
    },
    async (id: string, profile: Profile, done: Function) => {
      try {
        const account = await new AccountService(null)
          .findOne({
            openId: id
          });

        if (!account) {
          return done(null, false, {
              message: 'Invalid data'
            }
          )
        }

        return done(null, account);
      } catch (e) {
        return done(e);
      }
    }));

  app.post(getUrlAuth('openid'), passport.authenticate('openid'));
  app.get(getUrlCallback('openid'),
    passport.authenticate('openid', {
      successRedirect: 'http://localhost:4000/graphql',
      failureRedirect: 'http://localhost:4000/graphql'
    }));
};

export default includeOpenId;