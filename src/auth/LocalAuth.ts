import passport from "passport";
import {AccountService} from "../graphql/account/Account.service";
const {GraphQLLocalStrategy} = require('graphql-passport');

const includeLocalAuth = () => {
  passport.use(new GraphQLLocalStrategy(async (
    email: string,
    password: string,
    done: Function) => {
      try {
        const accountService = new AccountService(null);
        const res = await accountService.logIn({
          login: email,
          password
        });

        console.log(res);
        if (!res) {
          return done(null, false, {
            message: 'Invalid data'
          })
        }

        return done(null, res.account);
      } catch (e) {
        return done(e);
      }
    })
  );
};

export default includeLocalAuth;