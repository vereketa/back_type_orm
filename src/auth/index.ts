import includeFacebookAuth from "./FacebookAuth";
import includeGoogleAuth from "./GoogleAuth";
import includeOpenId from "./OpenIdAuth";
import includeLocalAuth from "./LocalAuth";
import app from '../server/app';
import passport from "passport";
import {Account} from "../DAL/models/Account";
import {AccountService} from "../graphql/account/Account.service";

const initializePassport = () => {
  passport.serializeUser((user: Account, done) => {
    const id = user.id;
    done(null, id);
  });

  passport.deserializeUser(async (id: number, done) => {
    console.log('deserializeUser: ', id);
    const account = await new AccountService(null).getOneById(id);
    done(null, account);
  });

  app.use(passport.initialize());
  app.use(passport.session());
};

export {includeFacebookAuth, includeGoogleAuth, includeLocalAuth, includeOpenId, initializePassport};