import passport from "passport";
import {OAuth2Strategy as GoogleStrategy} from "passport-google-oauth";
import {AccountService} from "../graphql/account/Account.service";
import {Account} from "../DAL/models/Account";
import config from '../../config';
import app from '../server/app';
import {getUrlAuth, getUrlCallback} from "../util/getUrlAuth";

const googleScopes = [
  'https://www.googleapis.com/auth/plus.login',
  'email',
  'profile',
  'openid'
];

const includeGoogleAuth = () => {
  passport.use(new GoogleStrategy({
      clientID: config.google.clientId,
      clientSecret: config.google.secret,
      callbackURL: config.google.callbackURL,

    },
    async (accessToken, refreshToken, profile, done) => {
      try {
        const accountService = new AccountService(null);
        let account = await accountService
          .findOne({
            googleId: profile.id
          });


        if (!account) {
          account = new Account();
          account.facebookId = profile.id;
          account.firstName = profile.name.givenName;
          account.lastName = profile.name.familyName;
          account.email = profile.emails &&
            profile.emails[0] &&
            profile.emails[0].value;

          account = await accountService.addAccount(account);
        }

        return done(null, account);
      } catch (e) {
        return done(e);
      }
    }));

  app.get(getUrlAuth('google'), passport.authenticate('google', {
    scope: googleScopes
  }));
  app.get(getUrlCallback('google'),
    passport.authenticate('google', {
      successRedirect: 'http://localhost:4000/graphql',
      failureRedirect: 'http://localhost:4000/graphql'
    }));
};

export default includeGoogleAuth;