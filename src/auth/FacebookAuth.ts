import {Strategy as FacebookStrategy, StrategyOptionWithRequest, VerifyFunctionWithRequest} from "passport-facebook";
import config from "../../config";
import {getUrlFull} from "../util/getUrlAuth";
import {AccountService} from "../graphql/account/Account.service";
import {Account} from "../DAL/models/Account";
import passport from "passport";


const facebookOptions: StrategyOptionWithRequest = {
  clientID: config.facebook.clientId,
  clientSecret: config.facebook.secret,
  callbackURL: getUrlFull('facebook'),
  passReqToCallback: true,
  profileFields: ['id', 'email', 'first_name', 'last_name']
};

const includeFacebookAuth = () => {
  const facebookCallback: VerifyFunctionWithRequest = async (req, accessToken, refreshToken, profile, done): Promise<any> => {
    const accountService = await new AccountService(null);
    let account = await accountService.findOne({
      facebookId: profile.id
    });


    if (!account) {
      account = new Account();
      account.facebookId = profile.id;
      account.firstName = profile.name.givenName;
      account.lastName = profile.name.familyName;
      account.email = profile.emails &&
        profile.emails[0] &&
        profile.emails[0].value;

      account = await accountService.addAccount(account);
    }
    done(null, account);
  };

  passport.use(new FacebookStrategy(
    facebookOptions,
    facebookCallback
  ));
};

export default includeFacebookAuth;