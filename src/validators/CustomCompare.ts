import {ValidationArguments, ValidatorConstraint, ValidatorConstraintInterface} from "class-validator";

@ValidatorConstraint({name: 'CustomCompare', async: true})
export class CustomCompare implements ValidatorConstraintInterface {
  defaultMessage(validationArguments?: ValidationArguments): string {
    return "Password must be equal!";
  }

  validate(value: any, validationArguments?: ValidationArguments): Promise<boolean> | boolean {
    const account: any = validationArguments.object;
    const name: string = validationArguments.constraints[0];
    return value === account[name];
  }
}