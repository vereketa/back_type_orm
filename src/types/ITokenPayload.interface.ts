import {Unit} from "./Unit";
import {IAccount} from "./IAccount.interface";

export interface ITokenPayload {
  payload: IAccount;
  time: number;
  unit?: Unit;
}