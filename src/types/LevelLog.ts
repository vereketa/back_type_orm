export enum LevelLog {
  info = 'info',
  error = 'error',
  warning = 'warn'
}
