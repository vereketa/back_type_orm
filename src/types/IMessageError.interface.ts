export interface IMessageErrorInterface {
  message: string;
  status: number;
}