export enum Unit {
  Minute = 'm',
  Second = 's',
  Hour = 'h',
  Day = 'd'
}