import {Roles} from "../constants/roles";

export interface IAccount {
  id: number;
  roles?: Roles[];
  exp?: number;
}