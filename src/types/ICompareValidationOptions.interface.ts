import {ValidationOptions} from "class-validator";

export interface ICompareValidationOptions extends ValidationOptions{
  compareName: string;
}