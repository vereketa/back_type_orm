import {IAccount} from "./IAccount.interface";
import {Account} from "../DAL/models/Account";

export interface Context {
  account?: IAccount;
  session?: any;
}

export interface IContextPassport extends Context {
  isAuthenticated: () =>  boolean;
  isUnauthenticated: () => boolean;
  getUser: () => Account;
  authenticate: (name: string, options?: any) => any;
  login: (user: Account, options?: any) => any;
  logout: () => any;
}