import {Field, InputType} from "type-graphql";
import {Account} from "../../DAL/models/Account";
import {Roles} from "../../constants/roles";
import {CompareField} from "../../decorators/CompareField.decorator";

@InputType()
export class AccountLogin implements Partial<Account> {
  @Field({nullable: true})
  login?: string;

  @Field({nullable: true})
  email?: string;

  @Field()
  password: string;
}

@InputType()
export class AccountInput extends AccountLogin {
  @Field()
  @CompareField({compareName: 'password'})
  repeatPassword: string;
}

@InputType()
export class AdminAccountInput extends AccountInput {
  @Field(returns => [Roles], {
    nullable: true
  })
  roles?: Roles[];
}