import {Service} from "typedi";
import {Authorized, Ctx, Info, Mutation, Query, Resolver, ResolverInterface} from "type-graphql";
import {Account} from "../../DAL/models/Account";
import {BaseResolver} from "../common/Base.resolver";
import {AccountService} from "./Account.service";
import {Arg} from "type-graphql/dist/decorators/Arg";
import {AccountInput, AccountLogin, AdminAccountInput} from "./Account.input";
import {AccountReturn, AccountToken} from "./Account.type";
import {Roles} from "../../constants/roles";
import {WhiteList} from "../../decorators/WhiteList.decorator";
import {Context, IContextPassport} from "../../types/Context.interface";

@Service()
@Resolver(of => Account)
export class AccountResolver extends BaseResolver(Account) {
  constructor(private readonly service: AccountService) {
    super();
  }

  @Authorized([Roles.ADMIN])
  @Mutation(returns => Account)
  async addAccount(@Arg('account') account: AdminAccountInput) {
    return await this.service.addAccount(account);
  }

  @Authorized([Roles.ADMIN])
  @Mutation(returns => Boolean)
  async deleteAccount(@Arg('id') id: number) {
    return await this.service.removeAccount(id);
  }

  @Mutation(returns => AccountReturn)
  @WhiteList
  async singUp(@Arg('accountSign') account: AccountInput, @Ctx() context: Context) {
    const res = await this.service.signUp(account);
    /*context.session.account = {
      id: res.account.id,
      token: res.tokens.refresh_token
    };*/
    return res;
  }

  @Mutation(returns => AccountReturn)
  @WhiteList
  async logIn(@Arg('accountLogin') account: AccountLogin) {
    return await this.service.logIn(account);
  }

  @Authorized([Roles.ADMIN])
  @Mutation(returns => Boolean)
  async setRoles(@Arg('id') id: number,
                 @Arg('roles', returns => [Roles]) roles: Roles[]) {
    return await this.service.setRoles(id, roles);
  }

  @Mutation(returns => AccountToken)
  async refreshToken(@Arg('refresh_token') token: string) {
    return await this.service.refreshToken(token);
  }


  @Query(returns => Account)
  async currentAccount(@Ctx() context: IContextPassport) {
    const b = context.isAuthenticated();
    const user = context.getUser();
    console.log(user);
    return user;
  }

  @Mutation(returns => Boolean)
  logout(@Ctx() context: any) {
    return context.logout();
  }

  @Mutation(returns => Account)
  async loginPass(@Arg('accLogin') account: AccountLogin,
                  @Ctx() context: IContextPassport) {
    const {user} = await context.authenticate('graphql-local', {
      email: account.login,
      password: account.password
    }) ;

    context.login(user);
    return user;
  }

  @Mutation(returns => AccountReturn)
  async passSign(@Arg('account') account: AccountInput,
                 @Ctx() context: IContextPassport,
                 @Info() info: any) :Promise<AccountReturn> {
    console.log(info);
    const ac = await this.singUp(account, context);
    context.login(ac.account);

    return ac;
  }
}
