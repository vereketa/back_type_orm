import {Field, ObjectType} from "type-graphql";
import {Account} from "../../DAL/models/Account";

@ObjectType()
export class AccountToken {
  @Field()
  access_token: string;

  @Field()
  refresh_token: string;

  @Field()
  expires: number;
}

@ObjectType()
export class AccountReturn {
  @Field()
  account: Account;

  @Field()
  tokens: AccountToken;
}