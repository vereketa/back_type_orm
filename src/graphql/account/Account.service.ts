import {Service} from "typedi";
import {BaseService} from "../common/Base.service";
import {Account} from "../../DAL/models/Account";
import {AccountInput, AccountLogin, AdminAccountInput} from "./Account.input";
import {plainToClass} from "class-transformer";
import {generateToken, verifyToken} from "../../util/tokens.util";
import {Unit} from "../../types/Unit";
import {AccountReturn, AccountToken} from "./Account.type";
import bc from 'bcrypt';
import {IAccount} from "../../types/IAccount.interface";
import {Roles} from "../../constants/roles";
import {LoggerSingle} from "../../services/Logger.service";
import {LevelLog} from "../../types/LevelLog";


@Service()
export class AccountService extends BaseService<Account> {
  private expAccess = {
    time: 30,
    unit: Unit.Minute
  };
  private expRefresh = {
    time: 60,
    unit: Unit.Day
  };

  constructor(private readonly logger: LoggerSingle) {
    super(Account);
  }

  async addAccount(accountInput: AdminAccountInput | Account) {
    const account = plainToClass(Account, accountInput);
    await this.repository.save(account);
    return account;
  }

  async removeAccount(id: number) {
    try {
      await this.repository
        .createQueryBuilder()
        .update(Account)
        .set({deleteAccountAt: new Date().getTime()})
        .where('id = :id', {id})
        .andWhere('deleteAccountAt IS NULL')
        .execute();
      return true;
    }
    catch (e) {
      this.logger && this.logger.log(e.message, LevelLog.error);
      return false;
    }
  }

  async setRoles(id: number, roles: Roles[]) {
    try {
      await this.repository
        .createQueryBuilder()
        .update(Account)
        .set({roles})
        .where('id = :id', {id})
        .execute();
      return true;
    }
    catch (e) {
      this.logger && this.logger.log(e.message, LevelLog.error);
      return false;
    }
  }
  async logIn(accountLogin: AccountLogin) {
    const account = await this.repository
      .createQueryBuilder('account')
      .where('account.email = :email', {email: accountLogin.email})
      .orWhere('account.login = :login', {login: accountLogin.login})
      .getOne();

    const match = await bc.compare(accountLogin.password, account.password);

    if (!account || !match) {
      throw {
        message: 'Invalid login or password',
        status: 404
      }
    }
    const tokens = this.getTokens({
      id: account.id,
      roles: account.roles
    }, {
      id: account.id
    });

    account.tokens.push(tokens.refresh_token);
    await this.repository.save(account);

    const res: AccountReturn = {
      account,
      tokens
    };

    return res;
  }

  async signUp(accountInput: AccountInput): Promise<AccountReturn> {
    accountInput.password = bc.hashSync(accountInput.password, 10);
    const account = plainToClass(Account, accountInput);

    await this.repository.save(account);

    const tokens = this.getTokens({
      id: account.id
    }, {
      id: account.id
    });

    account.tokens = [tokens.refresh_token];
    await this.repository.save(account);

    return {
      account,
      tokens
    };
  }

  async refreshToken(refresh_token: string) {
    const payload = await verifyToken(refresh_token);

    const account = await this.repository
      .findOne(payload.id);

    if(account.tokens.some(t => t !== refresh_token)) {
      account.tokens = [];
      await this.repository.save(account);
      throw {
        message: 'logout'
      }
    }

    const {roles, id} = account;

    account.tokens = account.tokens.filter(t => t !== refresh_token);
    const tokens = this.getTokens({
      id,
      roles
    }, {
      id
    });
    account.tokens.push(tokens.refresh_token);

    await this.repository.save(account);
    return tokens;
  }

  private getTokens(access_payload: IAccount, refresh_payload: IAccount): AccountToken {
    const access_token = generateToken({
      payload: access_payload,
      ...this.expAccess
    });
    const {exp} = verifyToken(access_token);

    const refresh_token = generateToken({
      payload:refresh_payload,
      ...this.expRefresh
    });

    return {
      expires: exp * 1000,
      refresh_token,
      access_token
    }
  }
}

