import {
  Arg,
  Args,
  Authorized,
  Ctx,
  FieldResolver,
  Mutation,
  Query,
  Resolver,
  ResolverInterface,
  Root
} from "type-graphql";
import {Recipe} from "../../DAL/models/Recipe";
import {RecipeInput} from "./Recipe.input";
import {Service} from "typedi";
import {RecipeService} from "./Recipe.service";

@Service()
@Resolver(Recipe)
export class RecipeResolver implements ResolverInterface<Recipe> {
  constructor(private readonly service: RecipeService) {
  }

  @FieldResolver()
  averageRating(@Root() recipe: Recipe) {
      return 10;
  }
  @Mutation(returns => Recipe)
  async addRecipe(
      @Arg('recipe') inputRecipe: RecipeInput
  ): Promise<Recipe> {
    return await this.service.addRecipe(inputRecipe);
  }

  @Mutation(returns => Boolean)
  async removeRecipe(@Arg('id') id: string) {
    return await this.service.removeRecipe(id);
  }
}
