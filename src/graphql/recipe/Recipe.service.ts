import {Service} from "typedi";
import {BaseService} from "../common/Base.service";
import {Recipe} from "../../DAL/models/Recipe";
import {plainToClass} from "class-transformer";
import {RecipeInput} from "./Recipe.input";

@Service()
export class RecipeService extends BaseService<Recipe> {
  constructor() {
    super(Recipe);
  }

  async addRecipe(inputRecipe: RecipeInput) {
    const recipe = plainToClass(Recipe, inputRecipe);
    await this.repository.save(recipe);
    return recipe;
  }

  async removeRecipe(id: string) {
    const res = await this.repository.delete(id);
    console.log(res);
    return true;
  }
}
