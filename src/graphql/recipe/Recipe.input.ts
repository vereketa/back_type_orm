import {Field, InputType} from "type-graphql";
import {Length, MaxLength, ArrayMaxSize} from "class-validator";
import {Recipe} from "../../DAL/models/Recipe";

@InputType()
export class RecipeInput implements Partial<Recipe>{
  @Field()
  @MaxLength(30)
  title: string;

  @Field({ nullable: true })
  @Length(3, 255, {
    message: 'Error texxxxxxxxxxxxt'
  })
  description?: string;

  @Field(type => [String])
  @ArrayMaxSize(30)
  ingredients: string[];
}
