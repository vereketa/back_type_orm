import {ArgsType, Field, Int} from "type-graphql";
import {Max, Min} from "class-validator";

@ArgsType()
export class RecipeArgs {
  @Field(type => Int, { defaultValue: 0})
  @Min(0)
  skip: number;

  @Field(type => Int)
  @Min(1)
  @Max(50)
  take: number = 25;

  @Field({nullable: true})
  title?: string;


  startIndex = this.skip;
  endIndex = this.skip + this.take;
}
