import {
  Authorized,
  Mutation,
  Publisher,
  PubSub,
  PubSubEngine,
  Resolver,
  ResolverFilterData,
  Root,
  Subscription
} from "type-graphql";
import {Service} from "typedi";
import {FavoritesService} from "./Favorites.service";
import {Actor} from "../../DAL/models/Actor";
import {Arg} from "type-graphql/dist/decorators/Arg";
import {ActorInput, MovieInput} from "./Favorites.input";
import {Movie} from "../../DAL/models/Movie";
import {Notification, NotificationPayload} from "../../DAL/models/Notification.type";
import {Themes} from "../../constants/pubSub";
import CurrentUser from "../../decorators/CurrentUser.decorator";
import {IAccount} from "../../types/IAccount.interface";

@Service()
@Resolver()
export class FavoritesResolver {
  constructor(private readonly service: FavoritesService) {}

  @Authorized('MODER')
  @Mutation(returns => Actor)
  async addActor(@Arg('actor') inputActor: ActorInput,
                 @Arg('movie') inputMovie: MovieInput,
                 @CurrentUser() user: IAccount,
                 @PubSub() pubSub: PubSubEngine) {
    console.log('User: ' + user.id);

    const payload: NotificationPayload = {
      message: `Actor: ${inputActor.name}`,
      id: 1,
    };
    await pubSub.publish(Themes.ACTOR, payload);
    return await this.service.addActor(inputActor);
  }

  @Mutation(returns => Movie)
  async addMovie(@Arg('movie') inputMovie: MovieInput,
                 @PubSub(Themes.MOVIE) publish: Publisher<NotificationPayload>) {
    await publish({
      message: inputMovie.name,
      id: 1
    });
    return await this.service.addMovie(inputMovie);
  }

  @Subscription({
    topics: ({args}) => args.topic,
    filter: (resolverFilterData: ResolverFilterData<NotificationPayload, Themes>) => {
      console.log('resolverFilterData: ' + resolverFilterData);
      console.log(resolverFilterData);
      return true;
    }

  })
  newNotification(@Root() notificationPayload: NotificationPayload,
                  @Arg('topic', type => Themes) topic: Themes): Notification {
      return {
        ...notificationPayload,
        date: new Date()
      }
  }
}
