import {Inject, Service} from "typedi";
import {Actor} from "../../DAL/models/Actor";
import {Repository} from "typeorm";
import {Movie} from "../../DAL/models/Movie";
import {ActorInput, MovieInput} from "./Favorites.input";
import {plainToClass} from "class-transformer";
import {InjectRepository} from "typeorm-typedi-extensions";


@Service()
export class FavoritesService {
  constructor(@InjectRepository(Actor) private readonly reposActor: Repository<Actor>,
              @InjectRepository(Movie) private readonly reposMovie: Repository<Movie>) {
  }

  get repositoryActor() {
    return this.reposActor;
  }

  get repositoryMovie() {
    return this.reposMovie;
  }


  async addActor(inputActor: ActorInput) {
    const actor = plainToClass(Actor, inputActor);
    await this.repositoryActor.save(actor);
    return actor;
  }

  async addMovie(inputMovie: MovieInput) {
    const movie = plainToClass(Movie, inputMovie);
    await this.repositoryMovie.save(movie);
    return movie;
  }
}
