import {Field, InputType, Int} from "type-graphql";
import {Max, Min} from "class-validator";

@InputType({isAbstract: true})
abstract class FavoriteInput {
  @Field()
  name: string;

  @Field(returns => Int)
  userId: number;
}

@InputType()
export class ActorInput extends FavoriteInput {
  @Field(returns => Int)
  @Min(10)
  age: number;
}

@InputType()
export class MovieInput extends FavoriteInput {
  @Field(returns => Int)
  @Max(30)
  rating: number;
}
