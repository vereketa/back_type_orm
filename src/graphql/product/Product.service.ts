import {BaseService} from "../common/Base.service";
import {Product} from "../../DAL/models/Product.type";
import {Service} from "typedi";
import {ProductInput} from "./Product.input";
import {plainToClass} from "class-transformer";

@Service()
export class ProductService extends BaseService<Product> {
  constructor() {
    super(Product);
  }

  async loadProfile(product: Product) {
    return await this.repository
      .createQueryBuilder()
      .relation(Product, 'profile')
      .of(product)
      .loadOne();
  }

  async addProduct(productInput: ProductInput) {
    const product = plainToClass(Product, productInput);
    await this.repository.save(product);
    return product;
  }

  async updateProduct(id: number, productInput: ProductInput) {
    return await this.repository
      .createQueryBuilder()
      .update(Product)
      .set(productInput)
      .where('id = :id', {id})
      .execute();
  }
  async deleteProduct(id: number) {
    return this.repository
      .createQueryBuilder()
      .delete()
      .from(Product)
      .where('id = :id', {id})
      .execute();
  }
}