import {Field, InputType} from "type-graphql";
import {Categories} from "../../constants/categories";

@InputType()
export class ProductInput {
  @Field()
  name: string;

  @Field()
  price: number;

  @Field(type => Categories)
  categories: Categories;

  @Field()
  rating: number;

  @Field()
  count: number;

  @Field({nullable: true})
  profileId?: number;
}