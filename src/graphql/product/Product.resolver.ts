import {FieldResolver, Mutation, Resolver, ResolverInterface, Root} from "type-graphql";
import {Product} from "../../DAL/models/Product.type";
import {Service} from "typedi";
import {BaseResolver} from "../common/Base.resolver";
import {ProductService} from "./Product.service";
import {Arg} from "type-graphql/dist/decorators/Arg";
import {ProductInput} from "./Product.input";

@Service()
@Resolver(of => Product)
export class ProductResolver extends BaseResolver(Product) implements ResolverInterface<Product>{
  constructor(private readonly service: ProductService) {
    super()
  }

  @FieldResolver()
  async profile(@Root() product: Product) {
   return this.service.loadProfile(product);
  }

  @Mutation(returns => Product)
  async addProduct(@Arg('product') productInput: ProductInput) {
    return this.service.addProduct(productInput);
  }

  @Mutation(returns => Product)
  async updateProduct(@Arg('id') id: number,
                      @Arg('product') productInput: ProductInput) {
    return this.service.updateProduct(id, productInput);
  }

  @Mutation(returns => Boolean)
  async deleteProduct(@Arg('id') id: number) {
    return this.service.deleteProduct(id);
  }
}
