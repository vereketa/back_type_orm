import {GraphQLResolveInfo} from 'graphql';
import {ClassType, UseMiddleware} from "type-graphql";
import {getRepository, Repository} from "typeorm";
import {getQueryBuilder} from "../../util/getQueryBuilder";


export class BaseServiceFactory {
  static create(classType: ClassType) {
    return new BaseService(classType);
  }
}

export class BaseService<T> {
  protected repository: Repository<T>;

  constructor(protected classType: ClassType) {
    this.repository = getRepository(classType);
  }

  async getOneById(id: number) {
    return await this.repository.findOne(id);
  }

  async findOne(where: Partial<T>) {
    return await this.repository.findOne({
      where
    })
  }

  async getAll() {
    return await this.repository.find();
  }

  async deepQuery(info: GraphQLResolveInfo, id?: number) {
    const qb = await getQueryBuilder(this.classType.name, this.repository, info, id);
    if (id) {
      return await qb.getOne();
    } else {
      return await qb.getMany();
    }
  }
}
