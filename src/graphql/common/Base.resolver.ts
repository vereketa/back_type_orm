import {Arg, ClassType, Info, Int, Query, Resolver} from "type-graphql";
import {BaseService, BaseServiceFactory} from "./Base.service";
import {CacheControl} from "../../middlewares/CacheControl";
import getFieldList = require("graphql-list-fields");


export function BaseResolver(BaseCls: ClassType) {
  const prefix = BaseCls.name;

  @Resolver(of => BaseCls, { isAbstract: true })
  abstract class BaseResolverClass{
    protected service: BaseService<any>;

    constructor() {
      this.service = BaseServiceFactory.create(BaseCls);
    }

    @CacheControl({maxAge: 60})
    @Query(returns => BaseCls, { name: `getOne${prefix}`})
    public async getOne(@Arg('id', returns => Int,) id: number,
                        @Info() info: any) {
      const res = getFieldList(info);
      console.log(res);
      return await this.service.deepQuery(info, id);
    }

    @Query(returns => [BaseCls], { name: `getAll${prefix}`,
    complexity: options => {
      return options.childComplexity;
    }})
    public async getAll(@Info() info: any) {
      return this.service.deepQuery(info);
    }

  }

  return BaseResolverClass as any;
}
