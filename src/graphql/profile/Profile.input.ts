import {Field, InputType, Int} from "type-graphql";

@InputType()
export class AddProfileInput {
  @Field(type => Int)
  idUser: number;

  @Field()
  gender: string;

  @Field()
  photo: string;
}
