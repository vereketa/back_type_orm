import {Arg, FieldResolver, Info, Mutation, Resolver, ResolverInterface, Root} from "type-graphql";
import {Profile} from "../../DAL/models/Profile";
import {User} from "../../DAL/models/User";
import {AddProfileInput} from "./Profile.input";
import {Service} from "typedi";
import {ProfileService} from "./Profile.service";
import {Product} from "../../DAL/models/Product.type";
import getFieldList from "graphql-list-fields";
import {GraphQLResolveInfo} from 'graphql';

@Service()
@Resolver(of => Profile)
export class ProfileResolver{
  constructor(private readonly service: ProfileService){}

  @FieldResolver()
  async user(@Root() profile: Profile,
             @Info() info: GraphQLResolveInfo): Promise<User> {
    if(profile.user)
      return profile.user;
    return await this.service.loadUser(profile);
  }

  @Mutation(returns => Profile)
  async addProfile(@Arg('profile') inputProfile: AddProfileInput) {
    return await this.service.addProfile(inputProfile);
  }

  @FieldResolver()
  async products(@Root() profile: Profile): Promise<Product[]> {
    if(profile.products)
      return profile.products;
    return await this.service.loadProducts(profile);
  }
}
