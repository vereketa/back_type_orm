import {Inject, Service} from "typedi";
import {Profile} from "../../DAL/models/Profile";
import {getManager, Repository} from "typeorm";
import {AddProfileInput} from "./Profile.input";
import {plainToClass} from "class-transformer";
import {User} from "../../DAL/models/User";
import {InjectRepository} from "typeorm-typedi-extensions";
import DataLoader = require("dataloader");
import {Product} from "../../DAL/models/Product.type";

@Service()
export class ProfileService {
  private userLoader: DataLoader<number, User>;
  private productsLoader: DataLoader<number, Product>;


  constructor(@InjectRepository(Profile) private readonly repos: Repository<Profile>) {
    this.userLoader = new DataLoader<number, User>(async (ids: number[]) => {
      return await this.repository
        .createQueryBuilder()
        .relation(Profile, 'user')
        .of(ids[0])
        .loadMany();
    });
    this.productsLoader = new DataLoader<number, Product>(async (ids: number[]) => {
      const res =  await this.repository
        .createQueryBuilder()
        .relation(Profile, 'products')
        .of(ids[0])
        .loadMany();

      console.log(res);
      return res;
    })
  }
  get repository() {
    return this.repos;
  }

  async loadUser(profile: Profile) {
    return await this.repository
      .createQueryBuilder()
      .relation(Profile, 'user')
      .of(profile)
      .loadOne();
  }

  async loadProducts(profile: Profile) {
    return await this.repository
      .createQueryBuilder()
      .relation(Profile, 'products')
      .of(profile)
      .loadMany();
  }

  async addProfile(inputProfile: AddProfileInput) {
    const profile = plainToClass(Profile, inputProfile);
    await getManager().save(profile);

    await this.repository
      .createQueryBuilder()
      .relation(User, 'profile')
      .of(inputProfile.idUser)
      .set(profile.id);
    return profile;
  }
}
