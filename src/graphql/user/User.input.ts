import {Field, InputType, Int} from "type-graphql";
import {ISkill} from "../../DAL/models/ISkill";

@InputType()
class NameInput {
  @Field()
  first: string;

  @Field()
  last: string;
}
@InputType()
class SkillInput {
  @Field()
  name: string;

  @Field()
  level: string;
}

@InputType()
export class AddUserInput {
  @Field(type => NameInput, {
    nullable: true
  })
  name: NameInput;

  @Field(type => Int)
  age: number;

  @Field(type => [String])
  nicknames: String[];

  @Field(type => SkillInput)
  skills: SkillInput;
}
