import {User} from "../../DAL/models/User";
import {Arg, FieldResolver, Mutation, Resolver, ResolverInterface, Root, UseMiddleware} from "type-graphql";
import {AddUserInput} from "./User.input";
import {ResolveTime} from "../../middlewares/ResolveTime";
import {Inject, Service} from "typedi";
import {UserService} from "./User.service";
import {MovieActorUnion} from "../../DAL/models/MovieActor.union";
import {BaseResolver} from "../common/Base.resolver";


@Service()
@Resolver(of => User)
export class UserResolver extends BaseResolver(User) implements ResolverInterface<User> {
  constructor(private readonly service: UserService) {
    super();
  }

 /* @FieldResolver()
  async profile(@Root() user: User) {
    return await this.service.loadProfile(user);
  }*/

  @FieldResolver(returns => MovieActorUnion)
  async favorites(@Root() user: User) {
    return await this.service.loadFavorites(user);
  }
  @UseMiddleware(ResolveTime)
  @Mutation(returns => User)
  async addUser(@Arg('user') inputUser: AddUserInput) {
    return await this.service.addUser(inputUser);
  }
}
