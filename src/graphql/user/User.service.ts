import {Service} from "typedi";
import {User} from "../../DAL/models/User";
import {AddUserInput} from "./User.input";
import {plainToClass} from "class-transformer";
import {BaseService} from "../common/Base.service";
import DataLoader from "dataloader";
import {Profile} from "../../DAL/models/Profile";

@Service()
export class UserService extends BaseService<User> {
  private profileLoader: DataLoader<number, Profile>;

  constructor() {
    super(User);
    this.profileLoader = new DataLoader<number, Profile>(async (ids: number[]) => {
      return await this.repository
        .createQueryBuilder()
        .relation(User, 'profile')
        .of(ids[0])
        .loadMany();
    })
  }

  async loadFavorites(user: User) {
    return await this.repository
        .createQueryBuilder()
        .relation(User, 'favorites')
        .of(user)
        .loadMany();
  }

  async loadProfile(user: User) {
    return await this.repository
      .createQueryBuilder()
      .relation(User, 'profile')
      .of(user)
      .loadOne();
  }

  async addUser(inputUser: AddUserInput) {
    console.log('repository ', this.repository);

    const user = plainToClass(User, inputUser);
    await this.repository.save(user);
    return user;
  }
}
