interface IStorage {
  [name: string] : Array<any>;
}
export class StorageArray {
  private readonly storages: IStorage;
  private static instance: StorageArray;

  private constructor() {
    this.storages = {};
  }

  public static createInstance() {
    if(!StorageArray.instance) {
      StorageArray.instance = new StorageArray();
    }
    return StorageArray.instance;
  }

  public addItem(name: string, value: any) {
    if(!this.storages[name]) {
      this.storages[name] = [];
    }
    this.storages[name].push(value);
  }

  public getItem(name: string) {
    return this.storages[name];
  }
  public removeItem(name: string, value: any) {
    if(this.storages[name])
      this.storages[name] = this.storages[name].filter(item => item !== value);
  }

  public clear(name: string) {
    this.storages[name] = [];
  }
}
