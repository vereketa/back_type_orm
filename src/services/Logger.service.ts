import {Service} from "typedi";
import mkdirp from 'mkdirp';
import {LevelLog} from "../types/LevelLog";

const winston = require('winston');
const appRoot: any = require('app-root-path');

const {format} = winston;
const {combine, timestamp, prettyPrint} = format;

@Service()
export class LoggerSingle {
  private logger: LoggerService;
  constructor() {
    this.logger = LoggerService.getFileLogger();
  }

  log(message: string, level: LevelLog = LevelLog.info) {
    this.logger.log(message, level);
  }
}
export class LoggerService {
  private static instance: LoggerService;

  private logger: any;
  private constructor() {
    this.logger = this.getFileLogger();
    const date = new Date();
    const start = date.getTime();

    date.setHours(date.getHours() + 1);
    date.setMinutes(0,0,0);

    const end = date.getTime();

    setTimeout(() => {
      this.logger = this.getFileLogger();
      this.updateLogger();
    }, end - start);

  }
  public static getFileLogger = () => {
    if(!LoggerService.instance) {
      LoggerService.instance = new LoggerService();
    }
    return LoggerService.instance;
  };

  private months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

  private updateLogger = () => {
    const ms = 1000 * 60 * 60;
    const self = this;
    setTimeout(function update() {
      self.logger = self.getFileLogger();
      setTimeout(update, ms);
    }, ms)
  };

  private getFileLogName = () => {
    const date = new Date();
    date.setHours(date.getHours() + 3);
    const hour = date.getHours();
    const path = `${appRoot}/logs/${date.getFullYear()}/${this.months[date.getMonth()]}/${date.getDate()}`;
    const fullPath = `${path}/${hour}_00-${hour + 1}_00.log`;
    mkdirp.sync(path);
    return fullPath;
  };

  private getFileLogger = () => {
    const options = {
      level: 'info',
      filename: this.getFileLogName(),
      handleExceptions: true,
      json: true,
      maxsize: 5242880, // 5MB
      maxFiles: 4,
      colorize: false,
    };
    const logger = winston.createLogger({
      format: combine(
          timestamp(),
          prettyPrint()
      ),
      transports: [
        new winston.transports.File(options)
      ],
      exitOnError: false
    });

    return logger;
  }

  static getConsoleLogger = () => {
    const options = {
      level: 'debug',
          handleExceptions: true,
          json: false,
          colorize: true,
    };
    const logger = winston.createLogger({
      format: combine(
          timestamp(),
          prettyPrint()
      ),
      transports: [
          new winston.transports.Console(options)
      ],
      exitOnError: false
    });

    logger.stream = {
      write: (message: any) => {
        logger.info(message);
      }
    } as any;

    return logger;
  };

  log(message: string, level: LevelLog = LevelLog.info) {
    this.logger.log({
      level,
      message
    });
  }
}
