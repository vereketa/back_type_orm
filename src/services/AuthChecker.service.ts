import {AuthChecker} from "type-graphql";
import {Context} from "../types/Context.interface";

export const customAuthChecker: AuthChecker<Context> = (
    {root, args, context, info},
        roles,
) => {
  return true;//todo
  const { account } = context;

  if(roles.length === 0) {
    return account !== undefined;
  }

  if(!account) {
    return false;
  }

  return account.roles && account.roles.some((role: string) => roles.includes(role));
};
