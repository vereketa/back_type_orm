import "reflect-metadata";
import {createConnection, getRepository, Repository, useContainer} from "typeorm";
import express from 'express';
import {ApolloServer} from 'apollo-server-express';
import {buildSchema} from "type-graphql";
import {Container} from 'typedi';
import {fieldConfigEstimator, getComplexity, simpleEstimator} from "graphql-query-complexity";
import { GraphQLSchema, separateOperations} from "graphql";
import http from 'http';
import {customAuthChecker} from "./services/AuthChecker.service";
import {Context} from "./types/Context.interface";
import {ApolloEngine} from 'apollo-engine';
import {LoggerService} from './services/Logger.service';
import config from '../config';
import {verifyToken} from "./util/tokens.util";
import {StorageArray} from "./services/StorageArray.service";
import {ExpressContext} from "apollo-server-express/dist/ApolloServer";
import {ApolloServerPlugin} from "apollo-server-plugin-base";
import {User} from "./DAL/models/User";
import cors from 'cors';
import flash from 'connect-flash';
import bodyParser from "body-parser";
import cookieParser from 'cookie-parser';

const {buildContext} = require('graphql-passport');
const morgan = require('morgan');

/*const RedisStore = connectRedis(session);
const client = createClient();*/

const port = config.port;
const graphqlEndpointPath = "/graphql";
const PATH_RESOLVERS = __dirname + '/graphql/**/*.ts';

const app = express();
useContainer(Container);

/*const sessionStore = session({
  store: new RedisStore({
    url: config.redis.url,
    client
  }),
  secret: config.redis.secret,
  resave: false,
  saveUninitialized: false,
  cookie: {
    maxAge: 60 * 60 * 1000,
    httpOnly: false,
  },
});*/

app.use(cors());
app.use(flash());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(morgan('combined',
  {stream: LoggerService.getConsoleLogger().stream}));

//app.use(sessionStore);

async function startServer() {
  const connection = await createConnection();
  await getRepository(User);

  const schema = await getSchema();

  const server = new ApolloServer({
    schema,
    playground: true,
    tracing: false,
    cacheControl: {
      defaultMaxAge: 5,
    },
    engine: false,
    subscriptions: {
      path: '/subscriptions',
    },
    context: (data) => ({
      ...buildContext(data),
      ...getContext(data)
    }),
    plugins: [
      getInstancePluginComplexity(schema),
    ],
  });
  const engine = new ApolloEngine({
    apiKey: config.engine_api_key
  });

  server.applyMiddleware({app, cors: false});
  const httpServer = http.createServer(app);
  server.installSubscriptionHandlers(httpServer);

  engine.listen({
    port,
    httpServer,
    graphqlPaths: [graphqlEndpointPath]
  }, () => {
    console.log(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`);
    console.log(`🚀 Subscriptions ready at ws://localhost:${port}${server.subscriptionsPath}`);
  });
}

function getInstancePluginComplexity(schema: GraphQLSchema): ApolloServerPlugin {
  return {
    requestDidStart: () => ({
      didResolveOperation({request, document}) {
        const complexity = getComplexity({
          schema,
          query: request.operationName
            ? separateOperations(document)[request.operationName]
            : document,
          variables: request.variables,
          estimators: [
            fieldConfigEstimator(),
            simpleEstimator({
              defaultComplexity: 1
            })
          ]

        });

        if (complexity >= 1000) {
          throw new Error(
            `Sorry, too complicated query! ${complexity} is over 20 that is the max allowed complexity.`,
          );
        }

        console.log("Used query complexity points:", complexity);
      }
    })
  }
}
async function getContext(data: ExpressContext) {
  return {
    session: data.req.session
  };

  const {operationName} = data.req.body;
  if (operationName === 'IntrospectionQuery') {
    return {};
  }

  const list = StorageArray.createInstance().getItem('whiteList');
  if (list.some(l => l.toLowerCase() === operationName.toLowerCase())) {
    return {};
  }

  const {authorization} = data.req.headers;

  const account = verifyToken(authorization);

  const ctx: Context = {
    account,
    session: data.req.session
  };

  return ctx;
}
async function getSchema() {
  return await buildSchema({
    resolvers: [PATH_RESOLVERS],
    dateScalarMode: "timestamp",
    emitSchemaFile: true,
    container: Container,
    authChecker: customAuthChecker,
  });
}

startServer();