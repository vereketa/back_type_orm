import jwt, {TokenExpiredError} from 'jsonwebtoken';
import config from '../../config';
import {Unit} from "../types/Unit";
import {IAccount} from "../types/IAccount.interface";
import {ITokenPayload} from "../types/ITokenPayload.interface";

const secret = config.secret;

export function generateToken({payload, time, unit = Unit.Minute}: ITokenPayload){
  return jwt.sign(payload, secret, {
    expiresIn: `${time}${unit}`
  })
}

export function verifyToken(token: string) : IAccount {
  try {
    const decoded = jwt.verify(token, secret) as IAccount;
    return decoded;
  } catch (e) {
    if (e instanceof TokenExpiredError ) {
      throw {
        message: e.message,
        status: 401
      }
    }
    throw  {
      message: e.message,
      status: 400
    }
  }
}

