import config from '../../config';

const getUrlAuth = (name: string) => {
  return `/${config.auth_path}/${name}`;
};

const getUrlCallback = (name: string) => {
  return `${getUrlAuth(name)}/${config.callback_path}`;
};

const getUrlFull = (name: string) => {
  return `${config.base_url}${getUrlCallback(name)}`;
};

export {getUrlCallback, getUrlAuth, getUrlFull};