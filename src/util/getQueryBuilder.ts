import {Repository, SelectQueryBuilder} from "typeorm";
import {GraphQLResolveInfo} from "graphql";
import {v1} from "uuid";
import getFieldList from "graphql-list-fields";


function arrayToObject(array: string[]) {
  const obj = {};

  for (let i = 0; i < array.length; i++) {
    const arr = array[i].split('.');

    arr.reduce(((previousValue: any, currentValue: string, index: number) => {
      if (index === arr.length - 1)
        return previousValue[currentValue] = null;
      return previousValue[currentValue] = previousValue[currentValue] ? previousValue[currentValue] : {};
    }), obj);
  }

  return obj;
}


async function getQueryBuilder<T>(name: string, repository: Repository<T>, info: GraphQLResolveInfo | string[], id?: number): Promise<SelectQueryBuilder<T>> {
  let array;
  if (Array.isArray(info))
    array = info;
  else {
    array = getFieldList(info);
  }
  const blackList = [name];
  const originalName = name;

  const obj = arrayToObject(array.filter(f => f.indexOf('.') > 0));

  const qb = repository.createQueryBuilder(name);
  qb.select(array.filter(f => f.indexOf('.') < 0).map(f => `${name}.${f}`));

  const buildQuery = function anom(obj: any) {
    if (Array.isArray(obj)) {
      obj.forEach(ob => {
        const prevName = name;
        let [key, value] = ob;
        if (value === null) {
          qb.addSelect(`${name}.${key}`);
        } else {
          let firstKey = key;
          if (blackList.some(item => item === key)) {
            key += v1();
          }

          try {
            qb.leftJoin(`${name}.${firstKey}`, key);
          } catch (e) {
            qb.expressionMap.joinAttributes.pop();
            qb.addSelect(Object.keys(value).map(v => `${name}.${key}.${v}`));
            //qb.addSelect(`${name}.${key}`);
            return;
          }
          blackList.push(key);
          name = key;
          anom(Object.entries(value));
        }
        name = prevName;
      })
    }
  };

    buildQuery(Object.entries(obj));

  if (id)
    qb.where(`${originalName}.id = :id`, {id});
  return qb;
}

export {getQueryBuilder};