import {registerEnumType} from "type-graphql";

export enum Themes {
  ACTOR = 'PubSub.Actor',
  MOVIE = 'PubSub.Movie'
}

registerEnumType(Themes, {
  name: 'Themes'
});