import {registerEnumType} from "type-graphql";

export enum Roles {
  ADMIN = 'ADMIN',
  MODERATOR = 'MODERATOR',
  USER = 'USER'
}


registerEnumType(Roles, {
  name: 'Roles'
});