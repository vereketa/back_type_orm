import {registerEnumType} from "type-graphql";

export enum Categories {
  PHONE = 1,
  TV= 2,
  FOOD = 4,
  CAR = 8
}

registerEnumType(Categories, {
  name: 'Categories'
});