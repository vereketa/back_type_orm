import {MigrationInterface, QueryRunner} from "typeorm";

export class AlbumRefactoring1570306704738 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE "album" RENAME COLUMN "name" to "title"');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner
          .query(`ALTER TABLE "album" RENAME COLUMN "title" to "name"`)
    }

}
