import {MigrationInterface, QueryRunner} from "typeorm";

export class PostRefactoring1570308926215 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "post" RENAME COLUMN "title" TO "name"`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "post" RENAME COLUMN "name" TO "title"`, undefined);
    }

}
