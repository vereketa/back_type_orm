import {Column, Entity, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {User} from "./User";
import {Field, Int, ObjectType, UseMiddleware} from "type-graphql";
import {Product} from "./Product.type";

@ObjectType()
@Entity()
export class Profile {
  @Field(type => Int)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  gender: string;

  @Field()
  @Column()
  photo: string;

  @Field(type => User)
  @OneToOne(type => User, user => user.profile)
  user: User;

  @Field(type => [Product])
  @OneToMany(type => Product, object => object.profile)
  products: Product[];
}
