import {Field, ID, Int, InterfaceType} from "type-graphql";
import {Gender} from "./Gender.enum";
import {User} from "./User";

@InterfaceType({
  resolveType: value => {
    if('age' in value) {
      return 'User';
    }
    return User;
  }
})
export abstract class IPerson {
  @Field(type => ID)
  id: number;

  @Field(type => Gender, {
    nullable: true,
    defaultValue: Gender.male
  })
  gender: Gender;

  @Field(type => Int)
  age: number;
}
