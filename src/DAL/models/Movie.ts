import {ChildEntity, Column} from "typeorm";
import {Field, Int, ObjectType} from "type-graphql";
import {Favorite} from "./Favorite";

@ObjectType()
@ChildEntity()
export class Movie extends Favorite {
  @Field(type => Int)
  @Column()
  rating: number;
}
