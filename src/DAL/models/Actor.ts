import {ChildEntity, Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import {Field, Int, ObjectType} from "type-graphql";
import {Favorite} from "./Favorite";

@ObjectType()
@ChildEntity()
export class Actor extends Favorite{
  @Field(type => Int)
  @Column()
  age: number;
}
