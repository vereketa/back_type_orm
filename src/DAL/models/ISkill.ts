import {Field, ObjectType} from "type-graphql";

@ObjectType()
export class ISkill {
  @Field()
  name: string;

  @Field()
  level: string;
}
