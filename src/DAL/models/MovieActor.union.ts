import {createUnionType} from "type-graphql";
import {Actor} from "./Actor";
import {Movie} from "./Movie";

export const MovieActorUnion = createUnionType({
  name: 'MovieActor',
  types: () => [Movie, Actor],
  resolveType: value => {
    if('rating' in value) {
      return Movie;
    }
    if('age' in value) {
      return 'Actor'
    }
    return undefined;
  }
});