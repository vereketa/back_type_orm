import {User} from "./User";
import {Column, Entity, ManyToOne, PrimaryGeneratedColumn, TableInheritance} from "typeorm";
import {Field, Int, ObjectType} from "type-graphql";

@Entity()
@ObjectType()
@TableInheritance({
  column: {
    type: 'varchar',
    name: 'type'
  }
})
export class Favorite {
  @Field()
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  name: string;

  @Column()
  userId: number;

  @ManyToOne(type => User, user => user.favorites)
  user: User;
}
