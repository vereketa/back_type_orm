import {Field, ID, ObjectType} from "type-graphql";
import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@ObjectType({
  description: 'The recipe model'
})
@Entity()
export class Recipe {
  @Field(type => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field({
    description: 'The title of the recipe'
  })
  @Column()
  title: string;

  @Field({nullable: true})
  @Column({
    nullable: true
  })
  description?: string;

  @Field({
    defaultValue: new Date()
  })
  @Column({
    nullable: true
  })
  creationDate?: Date;

  @Field(type => [String], {
    nullable: true
  })
  @Column('simple-array')
  ingredients: string[];

  @Field({nullable: true})
  averageRating?: number;
}
