import {Field, Int, ObjectType} from "type-graphql";
import {Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {Roles} from "../../constants/roles";

@ObjectType()
@Entity()
export class Account {
  @Field(type => Int)
  @PrimaryGeneratedColumn()
  id: number;

  @Field({nullable: true})
  @Column({unique: true, nullable: true})
  login?: string;

  @Field()
  @Column({unique: true})
  email: string;

  @Field()
  @Column({nullable: true})
  firstName: string;

  @Field()
  @Column({nullable: true})
  lastName: string;

  @Column({
    nullable: true
  })
  password?: string;

  @Field(type => [Roles], {
    defaultValue: null,
    nullable: true
  })
  @Column({
    type: 'simple-array',
    default: null,
    nullable: true
  })
  roles?: Roles[];

  @Column('jsonb', {
    default: []
  })
  tokens: string[];

  @Field({
    nullable: true,
    defaultValue: null
  })
  @Column("int8", {
    nullable: true,
    default: null
  })
  deleteAccountAt: number;


  @Field({
    nullable: true
  })
  @Column({unique: true, nullable: true})
  facebookId?: string;


  @Field({nullable: true})
  @Column({unique: true, nullable: true})
  openId: string;

  @Field({nullable: true})
  @Column({unique: true, nullable: true})
  googleId: string;
}