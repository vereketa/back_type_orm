import {registerEnumType} from "type-graphql";

export enum Gender {
  male,
  female
}

registerEnumType(Gender, {
  name: 'Gender',
  description: 'Gender type'
});