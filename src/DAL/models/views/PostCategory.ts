import {ViewColumn, ViewEntity} from "typeorm";
import {Category2} from "./Category";
import {Post} from "./Post";

@ViewEntity({
  expression: connection => connection.createQueryBuilder()
      .select('post.id', 'id')
      .addSelect('post.name', 'name')
      .addSelect('category.name', 'categoryName')
      .from(Post, 'post')
      .leftJoin(Category2, 'category', 'category.id = post.categoryId')
})
export class PostCategory {
  @ViewColumn()
  id: number;

  @ViewColumn()
  name: string;

  @ViewColumn()
  categoryName: string;
}
