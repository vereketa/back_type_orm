import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class Category2 {
  @PrimaryGeneratedColumn()
  id: number;
  
  @Column()
  name: string;
}
