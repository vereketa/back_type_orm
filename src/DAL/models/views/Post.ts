import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Category2} from "./Category";

@Entity()
export class Post {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  categoryId: number;
  
  @ManyToOne(type => Category2)
  @JoinColumn({
    name: 'categoryId'
  })
  category: Category2;
}
