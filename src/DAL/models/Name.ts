import {Column} from "typeorm";
import {Field, ObjectType} from "type-graphql";

@ObjectType()
export class Name {
  @Field()
  @Column()
  first: string;

  @Field()
  @Column()
  last: string;
}
