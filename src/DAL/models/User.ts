import {Favorite} from "./Favorite";
import {Entity, PrimaryGeneratedColumn, Column, Generated, OneToOne, JoinColumn, OneToMany} from "typeorm";
import {Name} from "./Name";
import {Profile} from "./Profile";
import {ISkill} from "./ISkill";
import {Authorized, Field, Int, ObjectType, UseMiddleware} from "type-graphql";
import {MovieActorUnion} from "./MovieActor.union";
import {IPerson} from "./IPerson";
import {Gender} from "./Gender.enum";
import {CacheControl} from "../../middlewares/CacheControl";


@ObjectType({implements: IPerson})
@Entity()
export class User extends IPerson{
    @Authorized('MODER')
    @PrimaryGeneratedColumn()
    id: number;

    @Authorized()
    @Column()
    @CacheControl({maxAge: 60})
    age: number;

    @Authorized('ADMIN')
    @Field(type => Gender)
    @Column({
        type: 'enum',
        enum: Gender,
        default: Gender.male,
        nullable: true
    })
    gender: Gender;

    @Field(type => Name, {complexity: 5})
    @Column(type => Name)
    name: Name;

    @Authorized('REGULAR')
    @Field(type => [String], {
        nullable: true,
    })
    @Column({
        type: "simple-array",
        default: [],
        nullable: true,
        transformer: {
            from(value: string): string[] {
                return value[0].split('|');
            },
            to(value: string[]): string {
                return value.join('|')
            }
        }
    })
    nicknames?: String[];

    @Field()
    @Column({
        type: "simple-json",
        nullable: true,
        transformer: {
            from(value: any): any {
                return JSON.stringify(value);
            },
            to(value: any): any {
                return value;
            }
        }
    })
    skills: string;

    @Field()
    @Column()
    @Generated('uuid')
    uuid: string;

    @Field(type => Int)
    @Column({
        nullable: true
    })
    profileId: number;

    @Field(type => Profile, {
        nullable: true
    })
    @OneToOne(type => Profile, profile => profile.user, {
        nullable: true
    })
    @JoinColumn()
    profile?: Profile;

    @Field(type => [MovieActorUnion], {
        nullable: true
    })
    @OneToMany(type => Favorite, favorite => favorite.user, {
        nullable: true
    })
    favorites?: Favorite[];
}
