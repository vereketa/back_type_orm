import {Field, ID, ObjectType} from "type-graphql";

@ObjectType()
export class Notification {
  @Field(type => ID)
  id: number;

  @Field({nullable: true})
  message?: string;

  @Field(type => Date)
  date: Date;
}

@ObjectType()
export class NotificationPayload {
  @Field()
  id: number;

  @Field()
  message?: string;
}