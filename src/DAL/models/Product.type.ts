import {Field, Int, ObjectType} from "type-graphql";
import {Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Categories} from "../../constants/categories";
import {Profile} from "./Profile";

@ObjectType()
@Entity()
export class Product {
  @Field(type => Int)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  name: string;


  @Field()
  @Column()
  price: number;

  @Field(type => Categories)
  @Column({
    type: 'enum',
    enum: Categories,
  })
  categories: Categories;

  @Field(type => Int)
  @Column()
  rating: number;

  @Field(type => Int)
  @Column()
  count: number;

  @Column({nullable: true})
  profileId?: number;

  @Field(type => Profile)
  @ManyToOne(type => Profile, object => object.products)
  profile: Profile;
}