import {EntityRepository, getCustomRepository, getManager, Repository} from "typeorm";
import {User} from "../test/User";
import {Profile} from "../test/Profile";

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  findByName(first: string, last: string) {
    return this.findOne({
      name: {
        first,
        last
      }
    })
  }
}

export async function useRepository() {
  const userRepository = getCustomRepository(UserRepository);
  const user = userRepository.create();
  user.age = 10;
  user.name = {
    last: 'last',
    first: 'first',
  };
  user.nicknames = [
    'nik'
  ];
  user.skills = {
    level: '5',
    name: 'js'
  };

  const profile = new Profile();
  profile.gender = 'gen';
  profile.photo = 'photo';

  await getManager().save(profile);
  user.profile = profile;
  await userRepository.save(user);

  const u = await userRepository.findByName('first', 'last');
  console.log(u);
}