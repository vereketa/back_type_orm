import {EntitySubscriberInterface, EventSubscriber, InsertEvent} from "typeorm";

@EventSubscriber()
export class AnySubscriber implements EntitySubscriberInterface {
  beforeInsert(event: InsertEvent<any>): Promise<any> | void {
    console.log(event.entity);
  }
}