import {EntitySubscriberInterface, EventSubscriber, InsertEvent} from "typeorm";
import {Movie} from "../models/Movie";


@EventSubscriber()
export class MovieSubscriber implements EntitySubscriberInterface<Movie> {
  listenTo(): Function {
    return Movie;
  }

  beforeInsert(event: InsertEvent<Movie>): Promise<any> | void {
    console.log(event.entity);
  }
}
