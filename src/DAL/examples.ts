//typeorm migration:create -n AlbumRefactoring
import {Brackets, getConnection, getRepository} from "typeorm";
import {Post} from "./models/views/Post";
import {User} from "./models/User";

async function insertQueryBuilder() {
  await getConnection()
      .createQueryBuilder()
      .insert()
      .into(User)
      .values([

      ])
      .execute();
}

async function updateQueryBuilder() {
  await getConnection()
      .createQueryBuilder()
      .update(User)
      .set({})
      .where('id = :id', {id: 1})
      .execute();
}

async function deleteQueryBuilder() {
  await getConnection()
      .createQueryBuilder()
      .delete()
      .from(User)
      .where('id = :id', {id: 1})
      .execute();
}

async function WhereParams() {
  await getConnection()
      .createQueryBuilder()
      .where('user.name IN (:...names)', {names: ['a','b','c']})


  await getConnection()
      .createQueryBuilder()

      .where('user.r = :r', {r: true})
      .andWhere(new Brackets(qb => {
        qb.where('', {})
            .orWhere('', {})
      }))
  // SELECT ... FROM users user
  // WHERE user.registered = true AND
  // (user.firstName = 'Timber' OR user.lastName = 'Saw')
}


async function subQuery() {
  const qb = await getRepository(User).createQueryBuilder('user');

  const users = qb
      .where('user.title IN ' + qb.subQuery().select('user.name').from(User, 'user').getQuery())
      .setParameter('re', true)
      .getMany();

  //
  //-----------------------------------------------
  //
  const posts = await getConnection()
      .getRepository(Post)
      .createQueryBuilder('post')
      .where(qb => {
        const subQuery = qb.subQuery()
            .select('user.name')
            .from(User, 'user')
            .where('user.re = :re')
            .getQuery();
        return 'post.title IN ' + subQuery;
      })
      .setParameter('re', true)
      .getMany();

  //
  //-------------------------------------------------
  //

  const userQb = await getConnection()
      .getRepository(User)
      .createQueryBuilder('user')
      .select('user.name')
      .where('user.re = :re', {
        re: true
      });

  const postsQb = await getConnection()
      .getRepository(Post)
      .createQueryBuilder('post')
      .where('post.title IN (' + userQb.getQuery() + ')')
      .setParameters(userQb.getParameters())
      .getMany();
}
