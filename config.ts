import {config} from 'dotenv';
config();

const obj = {
  secret: process.env.SECRET,
  engine_api_key: process.env.ENGINE_API_KEY,
  port: process.env.PORT,
  base_url: process.env.BASE_URL,
  auth_path: process.env.AUTH_PATH,
  callback_path: process.env.AUTH_CALBACK,


  redis: {
    url: process.env.REDIS_URL,
    secret: process.env.REDIS_SECRET
  },
  facebook: {
    clientId: process.env.FACEBOOK_CLIENT_ID,
    secret: process.env.FACEBOOK_SECRET,
    callbackURL: process.env.FACEBOOK_URL
  },
  google: {
    clientId: process.env.GOOGLE_CLIENT_ID,
    secret: process.env.GOOGLE_SECRET,
    callbackURL: process.env.GOOGLE_URL,
  }
};

export default obj;